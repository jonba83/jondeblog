<?php get_template_part('templates/partials/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="alert alert-warning">
    <?php _e('Sorry, no results were found.', 'sage'); ?>
  </div>
  <?php get_search_form(); ?>
<?php endif; ?>

<section class="masonry-posts wow animated fadeInUp" data-wow-offset="200">
	<div class="container">
		<div class="row">
			<div class="grid">
				<?php while (have_posts()) : the_post(); ?>
				  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>			

<?php the_posts_navigation(); ?>
