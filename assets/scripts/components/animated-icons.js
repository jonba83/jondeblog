var AnimatedIcons = $.extend({/* AnimatedIcons*/}, BaseComponent, {

	triggerSelector: '.js-animated-icon',

	init: function() {
		$(this.triggerSelector).each(this.bindEvents.bind(this));
	},

	bindEvents: function(index, element) {
		$(element).click((function(event) {
			$(element).toggleClass('js-active-icon');
		}).bind(this));
	}
	
});