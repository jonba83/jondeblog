var MasonryLayout = $.extend({/* MasonryLayout*/}, BaseComponent, {

	componentSelector: '.masonry-posts',
	itemContainer: '.grid',
	itemPostSelector: '.grid-item',


	init: function() {
		$(this.componentSelector).each(this.bindEvents.bind(this));
	},

	bindEvents: function(index, element) {
		$(this.componentSelector).imagesLoaded(( function(event) {
			$(this.itemContainer).masonry({
              itemSelector: this.itemPostSelector
          	});
		}).bind(this));
	}
	
});