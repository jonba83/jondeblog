var PostImageCaption = $.extend({/* PostImageCaption*/}, BaseComponent, {

	componentSelector: '.single-post .entry-content img',

	init: function() {
		$(this.componentSelector).not('.attachment-post-thumbnail').each(function(){
			$caption = $(this).attr('alt');
			$("<p class='feature-image-caption'>"+$caption+"</p>").insertAfter( $(this).parent('p') );

		});
	}

	
});