var SearchLayer = $.extend({/* SearchLayer*/}, BaseComponent, {

	triggerSelector: '.icon-search, .icon-close',

	init: function() {
		$(this.triggerSelector).each(this.bindEvents.bind(this));
	},

	bindEvents: function(index, element) {
		$(element).click((function(event) {
			$('.search-layer').fadeToggle();
		}).bind(this));
	}
	
});