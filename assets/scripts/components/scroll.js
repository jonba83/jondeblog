var Scroll = $.extend({/* Scroll*/}, BaseComponent, {

	componentSelector: '.scroll',

	init: function() {
		$(this.componentSelector).each(this.bindEvents.bind(this));
	},

	bindEvents: function(index, element) {
		$(element).click((function(event) {
			this.scrollTo(event.currentTarget);
		}).bind(this));
	},

	scrollTo: function(element) {

		var $href = $(element).attr('href');
		var $anchor = "";
		
		
		if ($href === "#") {
			$anchor = 'html, body';
		}else{
			$anchor = $href;
		}

		$('html, body').animate({
			scrollTop: $($anchor).offset().top-50
		}, 500);
	}
});