var ProgressBar = $.extend({/* ProgressBar*/}, BaseComponent, {

	componentSelector: '.progress-bar',
	componentContainer: window,

	init: function() {
		$(this.componentSelector).each(this.bindEvents.bind(this));
	},

	bindEvents: function(index, element) {
		$(this.componentContainer).scroll((function(event) {
			winHeight = $(this.componentContainer).height();
	        docHeight= $(document).height();
	        max = docHeight-winHeight;
	        value = $(this.componentContainer).scrollTop();
	        width = value/max*100;
	        width+="%";
	        
	        $(this.componentSelector).css({width:width});

		}).bind(this));
	}
	
});