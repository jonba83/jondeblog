var Base = {

	// Method to override by child component, usually to init binding component / view events
	init: function() {

	},

	// This method unbind all events and init the component / view
	reinit: function() {
		this.unbindAll();
		this.init();
	},

	// Method to unbind all events of the component / view (recursive method)
	unbindAll: function(domElement, recursive) {

		var element = recursive ? domElement : (typeof this.componentSelector !== 'undefined' ? this.componentSelector : this.viewSelector);

		$(element).unbind();
		$(element).children().each((function(index, element) {
			this.unbindAll($(element), true);
		}).bind(this));
	}
};