var Utils = {

	scrollTo: function(element, time) {

		$('html, body').animate({
			scrollTop: $(element).offset().top
		}, time);
	}
};