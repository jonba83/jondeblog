<?php get_template_part('templates/partials/page', 'header'); ?>

<?php if (!have_posts()) : ?>
  <div class="container">
  	<div class="alert alert-warning">
    	<?php _e('Sorry, no results were found.', 'sage'); ?>
  	</div>
  	<?php get_search_form(); ?>
  </div>
<?php endif; ?>

<?php if (have_posts()) : ?>
<section class="masonry-posts">
	<div class="container">
		<div class="row">
			<div class="grid">
				<?php while (have_posts()) : the_post(); ?>
				  <?php get_template_part('templates/content', 'search'); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
