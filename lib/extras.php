<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');



/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Show highlights in home
 */
function highlights($offset) {
  
  $args = array( 'numberposts' => 1, 'offset' => $offset );
  $recent_posts = wp_get_recent_posts( $args );

  // check if it is the main highlight
  if($offset == 0){
    $article_class = "main";
  }else{
    $article_class = "secondary";
  }
  

  foreach( $recent_posts as $recent ){
    
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($recent["ID"]), 'full' );
    $url = $thumb['0'];
    $title = $recent['post_title'];
    $id = $recent['ID'];
    $cat = get_the_category($id);
    $category_name = $cat[0]->cat_name;
    $category_url = get_category_link($cat[0]->term_id);

    include(locate_template('templates/content-highlights.php'));
    
  }

}

/**
 * Show masonry posts in home
 */

function masonryPostsHome() {

  $args = array( 'numberposts' => 6, 'offset' => 3 );
  $recent_posts = wp_get_recent_posts( $args );

  foreach( $recent_posts as $recent ){
    
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($recent["ID"]), 'medium' );
    $url = $thumb['0'];
    $title = $recent['post_title'];
    $id = $recent['ID'];
    $cat = get_the_category($id);
    $category_name = $cat[0]->cat_name;
    $category_url = get_category_link($cat[0]->term_id);
    $excerpt_full = $recent['post_excerpt'];
    $excerpt = substr($excerpt_full,0,140);
    $excerpt = $excerpt."...";
    
    include(locate_template('templates/content-masonry.php'));
    
  }
}

/**
 * Show rest of Posts Home
 */

function restOfPostsHome() {

  $args = array( 'numberposts' => 100, 'offset' => 9 );
  $recent_posts = wp_get_recent_posts( $args );

  foreach( $recent_posts as $recent ){
    $title = $recent['post_title'];
    $id = $recent['ID'];
    $cat = get_the_category($id);
    $category_name = $cat[0]->cat_name;
    $category_url = get_category_link($cat[0]->term_id);
    $excerpt_full = $recent['post_excerpt'];
    $excerpt = substr($excerpt_full,0,150);
    $excerpt = $excerpt."...";
    
    include(locate_template('templates/content-rest-of-posts.php'));
    
  }
}

/**
 * Echoes the featured image URL
 * @param mixed $id
 */
function featureImageUrl($id = false) {
  $id = (!$id) ? get_the_ID() : $id;
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'full' );
  echo $thumb[0];
}


/**
 * Echoes the post category
 * @param mixed $id
 */
function postCategory($id = false) {
    $id = (!$id) ? get_the_ID() : $id;
    $categories = get_the_category($id);
    $separator = ' ';
    $output = '';
    if ( ! empty( $categories ) ) {
        foreach( $categories as $category ) {
            $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" class="post-category" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
        }
        echo trim( $output, $separator );
    }
}

/**
 * Echoes post excerpt
 * @param mixed $id
 */
function postExcerpt($id = false) {
  $id = (!$id) ? get_the_ID() : $id;
  $excerpt_full = get_the_excerpt($id);
  $excerpt = substr($excerpt_full,0,140);
  $excerpt = $excerpt."...";
  echo $excerpt;
}