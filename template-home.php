<?php
/**
 * Template Name: Home
 */

?>

<!-- Block Hero -->
<?php get_template_part('templates/partials/page', 'header'); ?>

<?php while (have_posts()) : the_post(); ?>

<!-- Highlights -->
<section class="highlights header-scroll-point">
	<div class="container">
		<div class="row">
			<div class="col-sm-7">
				<!-- Highlight 1-->
				<?php Roots\Sage\Extras\highlights(0); ?>
			</div>
			<div class="col-sm-5">
				<!-- Highlight 2-->
				<?php Roots\Sage\Extras\highlights(1); ?>
				<!-- Highlight 3-->
				<?php Roots\Sage\Extras\highlights(2); ?>
			</div>
		</div>
	</div>
</section>

<!-- Masonry posts -->
<section class="masonry-posts">
	<div class="container">
		<div class="row">
			<div class="grid">
				<?php Roots\Sage\Extras\masonryPostsHome(); ?>
			</div>
		</div>
	</div>
</section>

<!-- Rest of posts -->
<section class="rest-of-posts">
	<?php Roots\Sage\Extras\restOfPostsHome(); ?>
</section>

<?php endwhile; ?>