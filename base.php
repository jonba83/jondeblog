<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
<?php get_template_part('templates/partials/head'); ?>
<body <?php body_class(); ?>>
<!--[if IE]>
<div class="alert-ie">
  <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
</div>
<![endif]-->
  <?php get_template_part('templates/partials/menu'); ?>
  <?php get_template_part('templates/partials/search-layer'); ?>
  <?php
    do_action('get_header');
    get_template_part('templates/partials/header');
  ?>

  <div id="sb-site" class="wrapper-page-content">
      
      <main class="main">
          <?php include Wrapper\template_path(); ?>
      </main>
      
      <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside>
      <?php endif; ?>


      <?php
        do_action('get_footer');
        get_template_part('templates/partials/footer');
        wp_footer();
      ?>
  </div>
</body>
</html>
