<div class="search-layer">
	<div class="search-layer-inner">
		<form role="search" method="get" class="search-form" action="/">
			<input type="search" class="search-field" placeholder="Buscar..." value="" name="s" autofocus>
			<input type="submit" class="search-submit" value="Buscar">
		</form>
		<i class="icon-close"></i>
	</div>
</div>