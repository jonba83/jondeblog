<?php use Roots\Sage\Titles; ?>

<section class="page-header">
 	<div class="container">
  		<?php if ( is_page_template( "template-home.php")) { ?>
  			<h1 class="animated fadeInLeft">
  				<img class="logo" src="http://www.jonbarral.com/wordpress/wp-content/uploads/2016/07/logo_jondeblog_2.png" alt="Jondeblog logo">
  				<span><?php bloginfo('name'); ?></span>
  			</h1>
	  		<h2 class="animated fadeInRight"><?php bloginfo('description'); ?></h2>
  		<?php } else { ?>
  			<h1><?= Titles\title(); ?></h1>
  		<?php } ?>
	</div>	
</section>