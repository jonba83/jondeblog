<header class="header navbar-fixed-top sb-slide">
  	<button class="animated-icon-menu js-animated-icon sb-toggle-left">
  		<span>Toggle Menu</span>
  	</button>
  	
  	<a href="<?= esc_url(home_url('/')); ?>">
  		<img class="logo" src="http://www.jonbarral.com/wordpress/wp-content/uploads/2016/07/logo_jondeblog_2.png" alt="<?php bloginfo('name'); ?> logo">
  	</a>

    <i class="icon-search"></i>
	
  <?php if ( is_single() ) { ?> 
  		<p class="title-post"><?php the_title(); ?></p>
  		<div class="progress-bar"></div>
  <?php } ?>
</header>
