<div class="menu sb-slidebar sb-width-narrow sb-left">
	<a href="<?= esc_url(home_url('/')); ?>" class="menu-home-link"><i class="icon-home"></i> <?php _e("Portada", "jondeblog"); ?></a>
	<h3><?php _e("Categorías", "jondeblog"); ?></h3>
	<nav class="nav-primary">
		<?php
			if (has_nav_menu('primary_navigation')) :
				wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
			endif;
		?>
    </nav>
</div>