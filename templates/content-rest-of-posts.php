<article class="col-lg-4 col-md-6 wow animated fadeInUp <?= $i ?>">
	<div class="rest-of-posts-content">
		<a href="<?= $category_url ?>" class="rest-of-posts-category"><?= $category_name ?></a>
		<h2>
			<a href="<?= get_permalink( $id ); ?>" title="<?= $title ?>"><?= $title ?></a>
		</h2>
		<p><?= $excerpt ?></p>
	</div>
</article>