<article class="grid-item wow animated fadeInUp">
	<div class="masonry-post">
		<a href="<?= get_permalink($id); ?>" class="masonry-post-thumbnail" title="<?= $title ?>">
		    <img src="<?= $url ?>" />
		</a>
		<div class="masonry-post-content">
		    <div class="masonry-post-category">
		    	<a href="<?= $category_url ?>"><?= $category_name ?></a>
		    </div>
		    <h2 class="tit-masonry-post">
			   <a href="<?= get_permalink( $id ); ?>" title="<?= $title ?>"><?= $title ?></a>
		    </h2>
		    <p><?= $excerpt ?></p>
		</div>
	</div>
</article>