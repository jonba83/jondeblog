<?php 
while (have_posts()) : the_post(); 
  
?>

<article class="post">
	<?php
  	/**
 	* Show posts header
 	*/
 	get_template_part('templates/post/post', 'header'); 
	?>
  
  	<div class="post-content">
      <?php
	  	/**
	 	* Show post metas
	 	*/
	 	get_template_part('templates/post/post', 'metas');


	 	/**
	 	* Content flexible
		*/
	 	get_template_part('templates/post/post', 'content');

	 	/**
	 	* Post tags
		*/
	 	the_tags( '<ul class="post-tags"><li>', '</li><li>', '</li></ul>');

		?>

  	</div>
 
</article>

<?php
	/**
 	* Navigation between posts
	*/
 	get_template_part('templates/post/post', 'navigation');
?>

<?php endwhile; ?>
