<article class="highlight-post wow animated fadeInUp <?= $article_class ?>">
	<a href="<?= get_permalink($id); ?>" title="<?= $title ?>" class="layer-link">
	    <img src="<?= $url ?>" />
	</a>
	<div class="caption">
		<div class="caption-inner">
		    <div class="caption-title-box">
		    	<h2 class="tit-highlight">
				   <a href="<?= get_permalink( $id ); ?>" title="<?= $title ?>"><?= $title ?></a>
			    </h2>
	    		<a href="<?= $category_url ?>" class="highlight-category"><?= $category_name ?></a>
	    	</div>
	    </div>
	</div>
</article>