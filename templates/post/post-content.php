<?php 
/**
 * Checks if there's any layout customisation
 */
if (have_rows('section_block') && get_field('custom_layout_active')) { 
    while (have_rows('section_block')) {
        the_row();
?>
		<section class="post-section <?php the_sub_field('bg_color');?> <?php the_sub_field('txt_color');?> <?php if(get_sub_field('bg_image')){ ?>min-height<?php } ?>" <?php if(get_sub_field('bg_image')){ ?>style="background-image:url('<?php the_sub_field('bg_image'); ?>')"<?php } if(get_sub_field('has_id')){ ?>id="<?php the_sub_field('link_id'); ?>"<?php } ?>>
    		<?php
    			$columns = get_sub_field('columns');
	            $postSectionHeadline = get_sub_field('post_section_title');
				
	            /**
			 	 * Controls and resets the PostSectionHeadlineConatiner variable
			 	 */
	            if ( empty($postSectionHeadline) || $postSectionHeadline === "") {
	            	$postSectionHeadlineContainer = "";
	            } else {
	            	$postSectionHeadlineContainer = "<h2 class='post-headline'>".$postSectionHeadline."</h2>";
	            }

	            switch($columns) {
                    /**
				 	 * If ONE column
				 	 */
                    case 'one':
                        echo "<div class='post-1-column'>";
                        	echo $postSectionHeadlineContainer;
                        	the_sub_field('content');
                        echo "</div>";
                        break;

                    /**
				 	 * If TWO NATIVE columns
				 	 */
                    case 'two-native':
                        echo "<div class='post-2-columns-native'>";
                        	echo $postSectionHeadlineContainer;
 							echo "<div class='post-2-columns-inner-content'>";
                        		the_sub_field('content');
                        	echo "</div>";
                        echo "</div>";
                        break;
                    
                    /**
				 	 * If TWO CUSTOM columns
				 	 */
                    case 'two-custom':
		                echo $postSectionHeadlineContainer;
                        echo "<div class='post-2-columns-custom'><div class='row'>";
                        	echo "<div class='col-sm-6'>";
                        		the_sub_field('left_col_content');
                        	echo "</div>";
                        	echo "<div class='col-sm-6'>";
                        		the_sub_field('right_col_content');
                        	echo "</div>";
                        echo "</div></div>";
                        break;
			    }
	            
    		?>
		</section>

<?php 
    } 
} else {
	echo "<div class='post-default'>";
		the_content();
	echo "</div>";
}
?>
