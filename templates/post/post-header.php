<?php 
namespace Roots\Sage\Extras;
?>
<header class="post-header" style="background-image:url('<?php featureImageUrl(false); ?>')">
	<div class="post-title-layer header-scroll-point">
	  <h1 class="post-title"><?php the_title(); ?></h1>
	</div>
</header>