<?php
	namespace Roots\Sage\Extras;
?>
<nav class="post-navigation">
	<?php
		$prev_post = get_previous_post();
		$prev_post_id = $prev_post->ID;

		if (!empty( $prev_post )): ?>
			<div class="post-navigation-box">
				<a href="<?php the_permalink($prev_post_id); ?>" rel="prev" style="background-image:url('<?php featureImageUrl($prev_post_id); ?>')" title="<?php _e('Previous post', 'sage') ?>">
					<div class="post-navigation-caption">
						<h3><?php echo $prev_post->post_title ?></h3>
						<p><?php postExcerpt($prev_post_id); ?></p>
					</div>
				</a>
			</div>
	<?php endif;

		$next_post = get_next_post();
		$next_post_id = $next_post->ID; 

		if (!empty( $next_post )): ?>
			<div class="post-navigation-box">
				<a href="<?php the_permalink($next_post_id); ?>" rel="next" style="background-image:url('<?php featureImageUrl($next_post_id); ?>');" title="<?php _e('Next post', 'sage') ?>">
					<div class="post-navigation-caption">
						<h3><?php echo $next_post->post_title ?></h3>
						<p><?php postExcerpt($next_post_id); ?></p>
					</div>
				</a>
			</div>
	<?php endif ?>

</nav>