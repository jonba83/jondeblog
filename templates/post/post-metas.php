<?php 
namespace Roots\Sage\Extras; 
?>
<div class="post-metas">
	<i class="icon-category"></i> <?php postCategory(false); ?>
	<i class="icon-calendar"></i> <span class="post-date"><?php the_date('j F Y'); ?></span>
</div>