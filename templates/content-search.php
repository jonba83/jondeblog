<?php
	namespace Roots\Sage\Extras;
?>

<article class="grid-item wow animated fadeInUp">
	<div class="masonry-post">
		<a href="<?php the_permalink(); ?>" class="masonry-post-thumbnail" title="<?php the_title(); ?>">
		   <?php the_post_thumbnail(); ?>
		</a>
		<div class="masonry-post-content">
		    <div class="masonry-post-category">
		    	<?php postCategory(); ?>
		    </div>
		    <h2 class="tit-masonry-post">
			   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		    </h2>
		    <p><?php postExcerpt(); ?></p>
		</div>
	</div>
</article>
